# Scraper pour appartement (plutôt à Nantes)

Ce scraper mutli-sources permet de récupérer les informations sur les appartements en location
dans les alentours de Nantes. Le but est de centraliser les infos dans une feuilles Google spreadsheet
afin de filtrer/trier/catégoriser/commenter plus facilement.

La recherche se base sur un budget de 500 à 1000€ pour un 2 ou 3 pièces.

# Installation

1. `pip install -r requirements.txt` dans un virtualenv

2. Copier le template spreadsheet depuis [ce template gdoc](https://docs.google.com/spreadsheets/d/1-2q0BUU38uf8ibnDOCz8C59Xv-KvMbfBnVKi4me_bfw/edit?usp=sharing)
puis copier la clé comprise dans l'url `https://docs.google.com/spreadsheets/d/[GDOC_KEY]/edit` dans `apt/settings.py`, votre document gsheet
doit être accessible au public par lien de partage.

3. Ajouter une clé d'api dans Google (voir [la documentation de scrapy](https://pygsheets.readthedocs.io/en/latest/authorization.html)) et nommer le fichier `gdoc_api_key.json`

# Utilisation

Pour lancer le scraper, executer dans l'environnement : `scrapy crawl [spider]`

Liste des scrapers :

 - leboncoin_private
 - leboncoin_pro
 - seloger
 - avendrealouer
 - acheterlouer

Ainsi qu'une foultitude d'autres pour chaque agence immobilière, pour lesquelles il suffit d'éxécuter `python scraper-agences.py` afin de scraper toutes les agences.
