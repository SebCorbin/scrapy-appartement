from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from twisted.internet import defer, reactor

from apt.spiders.agences import *

configure_logging()
runner = CrawlerRunner()


@defer.inlineCallbacks
def crawl():
    yield runner.crawl(AccedSpider)
    yield runner.crawl(BeausejourSpider)
    yield runner.crawl(BlotSpider)
    yield runner.crawl(BrevilleSpider)
    yield runner.crawl(DeChampsavinSpider)
    yield runner.crawl(FonciaSpider)
    yield runner.crawl(GraslinSpider)
    yield runner.crawl(HemonSpider)
    yield runner.crawl(LAdresseSpider)
    yield runner.crawl(LaForetSpider)
    yield runner.crawl(LeRepereSpider)
    yield runner.crawl(MGeffraySpider)
    yield runner.crawl(MoisonSpider)
    yield runner.crawl(NexitySpider)
    yield runner.crawl(SchumanSpider)
    yield runner.crawl(ThierrySpider)
    yield runner.crawl(VivreIciSpider)
    reactor.stop()


crawl()
reactor.run()
