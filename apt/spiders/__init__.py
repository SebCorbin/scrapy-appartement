# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.
import re
from time import sleep

import pygsheets
import scrapy
from pygsheets import format_addr

from apt.settings import GDOC_KEY

REF = 0
DATE = 1
RENT = 2
SURFACE = 3
ENERGY_CLASS = 4
ROOMS = 5
KITCHEN = 6
PARKING = 7
EXTERIOR = 8
TITLE = 9
MAP = 10
FLOOR = 11
LIFT = 12
NB_FIELDS = 14


def detect_cuisine(body):
    """
    :param string body:
    :return:
    """
    flags = re.IGNORECASE ^ re.MULTILINE
    if re.search(r'cuisine', body, flags) is not None:
        r = []
        if re.search('am[é|e]nag[é|e]e?', body, flags) is not None:
            r.append('Amén')
        if re.search('[é|e]quip[é|e]e?', body, flags) is not None:
            r.append('É')
        if re.search('am[é|e]ricaine?', body, flags) is not None:
            r.append('Amér')
        if re.search('cuisine[^,]+s[é|e]par[é|e]e', body, flags) is not None:
            r.append('S')
        if re.search('ouverte', body, flags) is not None:
            r.append('O')
        return '?' if not r else '/'.join(r)
    return ''


def detect_parking(body):
    flags = re.IGNORECASE ^ re.MULTILINE
    if re.search(r'parking', body, flags) is not None or \
            re.search(r'garage', body, flags) is not None or \
            re.search(r'box', body, flags) is not None or \
            re.search(r'stationnement', body, flags) is not None:
        if re.search('ext[é|e]rieur', body, flags) is not None:
            return 'Extérieur'
        if re.search('sous[-\s]sol', body, flags) is not None:
            return 'Sous-sol'
        if re.search('parking couverte?', body, flags) is not None:
            return 'Couvert'
        return 'Oui'
    return ''


def detect_exterior(body):
    flags = re.IGNORECASE ^ re.MULTILINE
    if re.search(r'balcon', body, flags) is not None:
        return 'Balcon'
    if re.search(r'terr?asse', body, flags) is not None:
        return 'Terrasse'
    if re.search(r'logg?ia', body, flags) is not None:
        return 'Loggia'
    return ''


class AbstractSpider(scrapy.Spider):
    skip_existing = True
    ws_title = None
    sh = None  # type: pygsheets.Worksheet
    # Index of the row to be added
    new_row_index = 2

    # List of refs already in the sheet
    previous_ads = []

    # List of refs that have been updated
    updated_ads = []

    # Number of new ads
    new_ads_counter = 0

    # CSS Selectors
    ad_selector = None
    next_page_selector = None

    def __init__(self, name=None, **kwargs):
        super().__init__(name, **kwargs)

        # Connect to google sheets
        gc = pygsheets.authorize(service_file='gdoc_api_key.json')
        self.sh = gc.open_by_key(GDOC_KEY).worksheet_by_title(self.ws_title)
        self.get_previous_ads()

    def get_previous_ads(self):
        # Get already entered refs
        refs = self.sh.get_col(1, include_tailing_empty=False)
        self.previous_ads = refs[1:]
        # Update the new row index from previous entered values
        self.new_row_index = len(self.previous_ads) + 2

    def parse(self, response):

        for item in response.css(self.ad_selector):
            # Get the reference of this ad, if it already exists update, else
            # get a new range at theend of the sheet
            current_ref, url = self.get_ad_info(item)
            if current_ref in self.previous_ads:
                self.updated_ads.append(current_ref)
                if self.skip_existing:
                    continue
                row_index = self.previous_ads.index(current_ref) + 2
            else:
                row_index = self.new_row_index
                self.new_row_index += 1
                self.new_ads_counter += 1
                sleep(.3)

            # Get the cell range to be updated for this apartment
            crange = '%s:%s' % (
                format_addr((row_index, 1)),
                format_addr((row_index, NB_FIELDS - 1))
            )

            yield response.follow(url, self.parse_apt, priority=1, meta={
                'crange': crange,
            })

        if self.next_page_selector:
            next_page = response.css(self.next_page_selector).extract_first()
            if next_page is not None:
                yield response.follow(next_page, callback=self.parse)

    def get_ad_info(self, item):
        raise NotImplementedError()

    def parse_apt(self, response):
        raise NotImplementedError()

    def closed(self, reason):
        if reason == 'finished':
            # Send notification
            if self.new_ads_counter:
                self.send_notification()

            # Mark all outdated ads as outdated
            self.mark_outdated()

    def send_notification(self):
        import os
        import platform
        if platform.system() == 'Darwin':
            os.system("""
                    osascript -e 'display notification "{}" with title "{}"'
                    """.format("%d nouvelle(s) annonce(s)" %
                               self.new_ads_counter, self.ws_title))

        # TODO notfication FreeMobile

    def mark_outdated(self):
        outdated_ads = set(self.previous_ads) - set(self.updated_ads)
        for outdated_ad in outdated_ads:
            self.log("%s is outdated" % outdated_ad)
            row_index = self.previous_ads.index(outdated_ad) + 2

            request = {
                "repeatCell": {
                    "range": {
                        "sheetId": self.sh.id,
                        "startRowIndex": row_index - 1,
                        "endRowIndex": row_index,
                        "startColumnIndex": 1,
                        "endColumnIndex": 13
                    },
                    "cell": {
                        "userEnteredFormat": {
                            "textFormat": {
                                "strikethrough": True,
                            }
                        }
                    },
                    "fields": "userEnteredFormat"
                }
            }
            self.sh.client.sheet.batch_update(self.sh.spreadsheet.id, request)
