import json

from apt.settings import FIREFOX_USER_AGENT
from apt.spiders import *

LBC_URL = (
    'https://www.leboncoin.fr/recherche/?category=10&region=18&cities=Nantes&'
    'owner_type=%s&furnished=2&real_estate_type=1,2&price=500-1000&rooms=2-max'
    '&square=50-max'
)


class LbcPrivateSpider(AbstractSpider):
    name = "leboncoin_private"
    start_urls = [LBC_URL % 'private']
    ws_title = "LeBonCoin Particuliers"
    # skip_existing = False

    custom_settings = {
        'ROBOTSTXT_OBEY': False,
        'USER_AGENT': FIREFOX_USER_AGENT
    }

    # CSS Selector for each ad
    ad_selector = '.react-tabs__tab-panel ul > li > a.clearfix'
    next_page_selector = '.googleafs + div li:last-child *::attr(href)'

    def get_ad_info(self, item):
        url = item.css('::attr(href)').extract()[0]
        current_ref = re.findall('\d+', url)[0]
        return current_ref, url

    def parse_apt(self, response):

        values = [None for _ in range(NB_FIELDS - 1)]
        json_val = response.xpath('/html/body/script[3]/text()').extract()[0]
        json_val = json.loads(json_val[19:])

        data = json_val['adview']

        values[REF] = '=HYPERLINK("%s";"%s")' % (
            data['url'], data['list_id']
        )
        values[DATE] = data['first_publication_date']
        values[RENT] = data['price'][0]

        attrs = data['attributes']
        for attr in attrs:
            if attr['key'] == 'rooms':
                values[ROOMS] = attr['value']
            if attr['key'] == 'square':
                values[SURFACE] = attr['value']
            if attr['key'] == 'energy_rate':
                values[ENERGY_CLASS] = attr['value'].upper()

        body = data['body']
        values[KITCHEN] = detect_cuisine(body)
        values[PARKING] = detect_parking(body)
        values[EXTERIOR] = detect_exterior(body)
        values[TITLE] = data['subject']

        values[FLOOR] = ''
        values[LIFT] = ''
        values[MAP] = ''
        if not data['location']['is_shape']:
            values[MAP] = '=HYPERLINK("%s";"Voir")' % (
                    'https://www.google.fr/maps/search/%s,%s/' %
                    (data['location']['lat'], data['location']['lng'])
            )

        crange = response.meta['crange']
        self.sh.update_values(crange, [values])
        title_cell_addr = format_addr((format_addr(crange)[0], TITLE + 1))
        title_cell = self.sh.cell(title_cell_addr)
        assert title_cell.value == data['subject']
        title_cell.simple = False
        title_cell.note = body


class LbcProSpider(LbcPrivateSpider):
    name = "leboncoin_pro"
    start_urls = [LBC_URL % 'pro']
    ws_title = "LeBonCoin Pro"


