from apt.spiders import *

SOURCE = 0
REF = 1
DATE = 2
RENT = 3
SURFACE = 4
ROOMS = 5
TITLE = 6
NB_FIELDS = 8


class AgenceSpider(AbstractSpider):
    ws_title = "Agences"
    custom_settings = {
        'ROBOTSTXT_OBEY': False,
    }

    new_row_index = 1
    refs = None

    ad_selector = ''
    next_page_selector = ''

    css_ref = ''
    css_url = ''
    css_surface = ''
    css_rent = ''
    css_rooms = ''
    css_title = ''
    css_body = ''
    css_dispo = ''

    def parse(self, response):
        for item in response.css(self.ad_selector):
            if self.skip_item(item):
                continue

            # Get the reference of this ad, if it already exists update, else
            # get a new range at the end of the sheet
            current_ref = self.get_ref(item)
            if current_ref in self.previous_ads:
                self.updated_ads.append(current_ref)
                continue
                # self.new_row_index = self.previous_ads.index(current_ref) + 2
                # row_index = self.new_row_index
            else:
                row_index = self.new_row_index
                self.new_row_index += 1
                self.new_ads_counter += 1

            url = self.get_url(item)

            # Get the cell range to be updated for this apartment
            crange = '%s:%s' % (
                format_addr((row_index, 1)),
                format_addr((row_index, NB_FIELDS - 1))
            )
            values = [None for _ in range(NB_FIELDS - 1)]

            values[SOURCE] = self.name
            values[REF] = '=HYPERLINK("%s";"%s")' % (url, current_ref)
            values[DATE] = self.get_date(item)

            values[SURFACE] = self.get_surface(item)
            values[RENT] = self.get_rent(item)
            values[ROOMS] = self.get_rooms(item)

            body = ''
            if self.css_body:
                body = item.css(self.css_body).extract_first().strip()
                body += ' '

            if self.css_dispo:
                body += item.css(self.css_dispo).extract_first().strip()

            values[TITLE] = self.get_title(item)

            self.sh.update_values(crange, [values])

            title_cell_addr = format_addr((format_addr(crange)[0], TITLE + 1))
            title_cell = self.sh.cell(title_cell_addr)
            assert title_cell.value == values[TITLE]
            title_cell.simple = False
            title_cell.note = body

        if self.next_page_selector:
            next_page = response.css(self.next_page_selector).extract_first()
            if next_page is not None:
                yield response.follow(next_page, callback=self.parse)

    def get_title(self, item):
        return item.css(self.css_title).extract_first().strip()

    def get_url(self, item):
        return item.css(self.css_url).extract_first()

    def get_ref(self, item):
        return item.css(self.css_ref).extract()[0]

    @staticmethod
    def get_date(item):
        return ''

    def get_surface(self, item):
        return self.extract(item, self.css_surface) if self.css_surface else ''

    def get_rent(self, item):
        return self.extract(item, self.css_rent) if self.css_rent else ''

    def get_rooms(self, item):
        return self.extract(item, self.css_rooms) if self.css_rooms else ''

    def get_previous_ads(self):
        # Get already entered refs
        self.refs = self.sh.get_values('A2', 'B2000')

        # Update the new row index from previous entered values
        self.new_row_index = len(self.refs) + 2

        # Filter by current keyword only
        self.previous_ads = [r[1] for r in self.refs if r[0] == self.name]

    def parse_apt(self, response):
        pass

    def get_ad_info(self, item):
        pass

    @staticmethod
    def extract(item, css_selector):
        return item.css(css_selector).extract_first().strip()

    def mark_outdated(self):
        outdated_ads = set(self.previous_ads) - set(self.updated_ads)
        for outdated_ad in outdated_ads:
            self.log("%s is outdated" % outdated_ad)
            row_index = self.refs.index([self.name, outdated_ad]) + 2

            request = {
                "repeatCell": {
                    "range": {
                        "sheetId": self.sh.id,
                        "startRowIndex": row_index - 1,
                        "endRowIndex": row_index,
                        "startColumnIndex": 1,
                        "endColumnIndex": NB_FIELDS - 1
                    },
                    "cell": {
                        "userEnteredFormat": {
                            "textFormat": {
                                "strikethrough": True,
                            }
                        }
                    },
                    "fields": "userEnteredFormat"
                }
            }
            self.sh.client.sheet.batch_update(self.sh.spreadsheet.id, request)

    def skip_item(self, item):
        return False


class FonciaSpider(AgenceSpider):
    name = 'foncia'
    start_urls = [
        'https://fr.foncia.com/location/nantes-44/appartement--maison/(params)/'
        'on/(surface_min)/45/(prix_max)/1000/(pieces)/2--3--4--5'
    ]

    ad_selector = 'article.TeaserOffer'
    next_page_selector = '.Pagination a:last-child::attr(href)'

    css_ref = '::attr(data-reference)'
    css_url = 'a.TeaserOffer-ill::attr(href)'
    css_surface = '[data-behat=surfaceDesBiens]::text'
    css_rent = '.TeaserOffer-price-num::text'
    css_rooms = '[data-behat=nbPiecesDesBiens]::text'
    css_title = 'h3 a::text'
    css_body = '.TeaserOffer-description::text'
    css_dispo = '.TeaserOffer-dispo::text'

    @staticmethod
    def get_date(item):
        date = item.css('span::attr(data-diffusion)').extract_first()
        return '%s/%s/%s' % (date[-2:], date[4:6], date[:4])

    def get_url(self, item):
        return 'https://fr.foncia.com' + super().get_url(item)


class ThierrySpider(AgenceSpider):
    name = 'thierry'
    start_urls = [
        'https://www.thierry-immobilier.fr/location/appartement--maison/'
        'beaulieu--centre-ville--gare-sud-malakoff--pont-du-cens-petit-port--'
        'proce-monselet--quartier-chantenay-sainte-anne--republique--ste-'
        'therese-beausejour--tortiere-donatien/tous/1000-euros-maximum?pieces'
        '%5B2%5D=2&pieces%5B3%5D=3&pieces%5B4%5D=4&surface_minimum=45'
    ]
    ad_selector = 'article.node-immobilier'
    next_page_selector = 'li.pager-next a::attr(href)'

    css_url = 'a::attr(href)'
    css_surface = '.teaser__additional-inner span::text'
    css_rent = '.teaser__price b::text'
    css_title = 'h3.teaser__title::text'
    css_body = '.teaser__body p::text'
    css_dispo = '.teaser__additional-inner span:last-child::text'

    def get_ref(self, item):
        url = item.css(self.css_url).extract_first()
        return re.findall('-(\d+)\?search_path', url)[0]

    def get_surface(self, item):
        return item.xpath('.//a/div[1]/div[3]/p/span['
                          './/i[contains(@class, "fa-expand")]]/text()[1]'
                          ).extract_first()

    def get_url(self, item):
        return 'https://www.thierry-immobilier.fr' + super().get_url(item)


class AccedSpider(AgenceSpider):
    name = 'acced'
    start_urls = [
        'http://www.acced-immobilier.com/immobilier/pays/locations/france.htm?'
        'ci=440109&idpays=250&idqfix=1&idtt=1&lang=fr&pres=prestige&'
        'px_loyerbtw=501%252f1000&surf_terrainmax=Max&surf_terrainmin=Min&'
        'surfacemax=Max&surfacemin=50&tri=d_dt_crea'
    ]
    ad_selector = 'div.recherche-annonces'

    css_url = 'a::attr(href)'
    css_rent = '.prix-annonce::text'
    css_title = '.big::text'

    def get_ref(self, item):
        return re.findall('(\d+)', item.css('::attr(id)').extract_first())[0]

    def get_surface(self, item):
        return item.css('.typo-action + p::text'
                        ).extract_first().split('-')[0].strip()

    def get_rooms(self, item):
        return item.css('.typo-action + p::text'
                        ).extract_first().split('-')[1].strip().replace(
            '\n', ' ').replace('\r', '')

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0]


class GraslinSpider(AgenceSpider):
    name = 'graslin'
    start_urls = [
        'http://www.graslin-immobilier.com/recherche,basic.htm?ci=440109&idq'
        'fix=1&idtt=1&idtypebien=1&nolotinventes=1&saisie=Nantes&tri=d_dt_crea'
    ]
    ad_selector = 'div.recherche-annonces'

    css_url = 'a::attr(href)'
    css_rent = '.prix-annonce::text'
    css_title = '.big::text'

    def get_ref(self, item):
        return re.findall('(\d+)', item.css('::attr(id)').extract_first())[0]

    def get_surface(self, item):
        return item.css('.span4 a .padding-bottom-10 p::text'
                        ).extract()[2].split('\r\n,\r\n')[0].strip()

    def get_rooms(self, item):
        rooms = item.css('.span4 a .padding-bottom-10 p::text').extract()[2]
        rooms = rooms.split('\r\n,\r\n')[1].strip().replace('\n', ' ')
        return re.findall('(\d+)', rooms.replace('\r', ''))[0]

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0].strip()


class BrevilleSpider(AgenceSpider):
    name = 'breville'
    start_urls = [
        'http://www.breville-immobilier.com/nantes-0-location-maison-'
        'appartement.htm?habitation1=Appartement&habitation2=Maison&'
        'Rechercher.x=61&Rechercher.y=16'
    ]

    ad_selector = 'table table table table[width="585"] table[width="585"]'
    next_page_selector = 'a.verdana11noirs::attr(href)'
    css_url = 'a::attr(href)'
    css_rent = '.arial16pourpre strong::text'

    def get_url(self, item):
        return 'http://www.breville-immobilier.com/' + super().get_url(item)

    css_title = 'a::text'

    def get_ref(self, item):
        url = item.css(self.css_url).extract_first()
        return re.findall('(\d+)-', url)[0]

    def get_surface(self, item):
        return item.css('.verdana11noir strong::text'
                        ).extract()[0].split('-')[1].strip()

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0]

    def skip_item(self, item):
        return len(item.css('img[src="Images/Filtre_loue.gif"]'))


class LaForetSpider(AgenceSpider):
    name = 'laforet'
    start_urls = [
        'http://www.laforet.com/louer/rechercher?localisation=Nantes+(44)&'
        'rayon=00&price_min=0&price_max=1000&surface_min=40&surface_max=Max&'
        'maison=on&appartement=on'
    ]

    ad_selector = 'ul.results-compact > li'
    css_ref = '::attr(data-asp)'
    css_url = 'a::attr(href)'
    css_rent = 'p.price::text'
    css_title = 'h3::text'
    css_surface = '.surface::text'
    css_rooms = '.roomsquantity::text'

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0]

    def get_url(self, item):
        return 'http://www.laforet.com' + super().get_url(item)

    def get_rooms(self, item):
        return re.findall('(\d+)', super().get_rooms(item))[0]


class MoisonSpider(AgenceSpider):
    name = 'moison'
    start_urls = [
        'http://www.nantes-location.fr/recherche/resultat/location?searchPost'
        '%5Btypebien%5D%5B%5D=APPARTEMENT&searchPost%5Btypebien%5D%5B%5D=MAISON'
        '&searchPost%5Bbudget%5D%5B0%5D=500&searchPost%5Bbudget%5D%5B1%5D=1050&'
        'searchPost%5Bnbchambres%5D=0&searchPost%5Bnbsallebain%5D=0&searchPost'
        '%5Bnbparking%5D=0&searchPost%5Breference%5D=&rechercher=Rechercher'
    ]

    ad_selector = '.content-search_result .search_item'
    next_page_selector = 'a[rel="next"]::attr(href)'
    css_ref = 'p.reference::text'
    css_url = 'a::attr(href)'
    css_rent = 'p.prix::text'
    css_title = '::attr(rel)'
    css_surface = ''
    css_rooms = '.nbchambres p::text'

    def get_ref(self, item):
        return re.findall('(\d+)', super().get_ref(item))[0]

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0]

    def get_rooms(self, item):
        return int(super().get_rooms(item)) + 1

    def get_url(self, item):
        return 'http://www.nantes-location.fr' + super().get_url(item)


class LeRepereSpider(AgenceSpider):
    name = 'lerepere'
    start_urls = [
        'http://www.lerepereimmobilier.fr/rechercher-un-bien/?status=location&'
        'location%5B%5D=nantes&min-area=47'
    ]

    ad_selector = 'article.property'
    css_ref = '.figure_infos a::attr(onclick)'
    css_url = 'h3 a::attr(href)'
    css_rent = 'p.price::text'
    css_title = 'h3 a::text'
    css_surface = 'i.fa-arrows + .figure2::text'
    css_rooms = 'i.fa-bed + .figure::text'

    def get_ref(self, item):
        return re.findall('(\d+)', super().get_ref(item))[0]

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0]

    def get_rooms(self, item):
        return int(super().get_rooms(item)) + 1


class DeChampsavinSpider(AgenceSpider):
    name = 'dechampsavin'
    start_urls = [
        'http://www.dechampsavin.net/catalog/advanced_search_result.php?action='
        'update_search&search_id=&C_28_search=EGAL&C_28_type=UNIQUE&C_28='
        'Location&cfamille_id=&C_65_search=CONTIENT&C_65_type=TEXT&C_65=44+'
        'NANTES&C_65_temp=44+NANTES&C_30=0&C_30_search=COMPRIS&C_30_type=NUMBER'
        '&C_30_MIN=&C_30_MAX=&C_30_loc=0&C_33_search=COMPRIS&C_33_type=NUMBER&'
        'C_33_MAX=&C_33_MIN=0&C_38_search=COMPRIS&C_38_type=NUMBER&C_38_MAX=&'
        'C_38_MIN=0&C_43_search=COMPRIS&C_43_type=NUMBER&C_43_MAX=&C_43_MIN=0&'
        'C_39_search=COMPRIS&C_39_type=NUMBER&C_39_MAX=&C_39_MIN=0&C_47_search='
        'COMPRIS&C_47_type=NUMBER&C_47_MAX=&C_47_MIN=0&C_36=0&C_36_search='
        'COMPRIS&C_36_type=NUMBER&C_36_MIN=&C_36_MAX=&C_1737_search=EGAL&'
        'C_1737_type=FLAG&C_1737=&keywords='
    ]

    ad_selector = '.bienListing'
    css_ref = '.meta__ref'
    css_url = 'h3.meta__title a::attr(href)'
    css_rent = '.meta__price b::text'
    css_title = 'h3.meta__title a::text'
    css_body = '.meta__desc::text'

    def get_ref(self, item):
        return re.findall('GES(\d+)-316', super().get_ref(item))[0]

    def get_rent(self, item):
        return re.findall('([\s\d]+)', super().get_rent(item))[0]

    def get_url(self, item):
        return 'http://www.dechampsavin.net' + super().get_url(item)[2:]


class VivreIciSpider(DeChampsavinSpider):
    name = 'vivreici'
    start_urls = [
        'http://www.vivreici.com/catalog/advanced_search_result.php?action='
        'update_search&search_id=1612755267706537&C_28_search=EGAL&C_28_type='
        'UNIQUE&C_28=Location&cfamille_id=&C_65_search=CONTIENT&C_65_type=TEXT&'
        'C_65=44+NANTES&C_65_temp=44+NANTES&C_30=0&C_30_search=COMPRIS&C_30_typ'
        'e=NUMBER&C_30_MIN=&C_30_MAX=&C_30_loc=0&C_33_search=COMPRIS&C_33_type='
        'NUMBER&C_33_MAX=&C_33_MIN=0&C_38_search=COMPRIS&C_38_type=NUMBER&C_38_'
        'MAX=&C_38_MIN=0&C_43_search=COMPRIS&C_43_type=NUMBER&C_43_MAX=&C_43_MI'
        'N=0&C_39_search=COMPRIS&C_39_type=NUMBER&C_39_MAX=&C_39_MIN=0&C_47_sea'
        'rch=COMPRIS&C_47_type=NUMBER&C_47_MAX=&C_47_MIN=0&C_36=0&C_36_search=C'
        'OMPRIS&C_36_type=NUMBER&C_36_MIN=&C_36_MAX=&C_1737_search=EGAL&C_1737_'
        'type=FLAG&C_1737=&keywords='
    ]

    def get_ref(self, item):
        ref = super(DeChampsavinSpider, self).get_ref(item)
        if re.findall('GES(\d+)-316', ref):
            return re.findall('GES(\d+)-316', ref)[0]
        return re.findall('Ref : (.+) -', ref)[0]

    def get_url(self, item):
        return 'http://www.vivreici.com' + item.css(self.css_url
                                                    ).extract()[0][2:]


class BeausejourSpider(AgenceSpider):
    name = 'beausejour'
    start_urls = [
        'http://www.beausejour-immobilier.fr/catalog/advanced_search_result.php'
        '?action=update_search&search_id=1612295289445295&action=update_search&'
        'search_id=1612295289445295&C_28=Vente&C_28_search=EGAL&C_28_type=UNIQU'
        'E&C_28_search=EGAL&C_28_type=UNIQUE&C_28=Location&C_65_search=CONTIENT'
        '&C_65_type=TEXT&C_65=&C_27_search=EGAL&C_27=&C_27_type=UNIQUE&C_33_sea'
        'rch=SUPERIEUR&C_33_type=NUMBER&C_33_MIN=&C_30_search=COMPRIS&C_30_type'
        '=NUMBER&C_30_MIN=&C_30_MAX='
    ]

    ad_selector = '.annonce_listing'

    css_ref = '.btn_tell_friend::attr(href)'
    css_url = 'a::attr(href)'
    css_title = 'h3.type::text'

    def get_ref(self, item):
        return re.findall('(\d+)', super().get_ref(item))[0]

    def get_url(self, item):
        return 'http://www.beausejour-immobilier.fr' + super().get_url(item)[2:]

    def get_title(self, item):
        return '-'.join(super().get_title(item).split('-')[:-1])


class SchumanSpider(AgenceSpider):
    name = 'schuman'
    start_urls = [
        'http://www.agence-schuman.com/location/'
    ]

    ad_selector = '.locations'

    css_ref = '::attr(id)'
    css_url = 'h2 a::attr(href)'
    css_title = 'h2 a::text'
    css_rent = 'h2 a::text'

    def get_ref(self, item):
        return re.findall('(\d+)', super().get_ref(item))[0]

    def get_url(self, item):
        return 'http://www.agence-schuman.com' + super().get_url(item)[2:]

    def get_title(self, item):
        title = super().get_title(item)
        return re.findall('Location\s+[a-z]+\s+(.+)\s+–', title)[0]

    def get_rent(self, item):
        rent = super().get_rent(item)
        return re.findall('–\s+(\d+)\s+€', rent)[0]


class HemonSpider(AgenceSpider):
    name = 'hemon'
    start_urls = [
        'http://www.cabinet-hemon.com/recherche,basic.htm?ci=440109&idqfix=1&id'
        'tt=1&idtypebien=1&lang=fr&pres=prestige&px_loyermax=1000&px_loyermin=M'
        'in&surf_terrainmax=Max&surf_terrainmin=Min&surfacemax=Max&surfacemin=M'
        'in'
    ]

    ad_selector = '.recherche-annonces-location'

    css_ref = 'a::attr(href)'
    css_url = 'a::attr(href)'
    css_title = 'span.quartier-annonce::text'
    css_body = 'span[itemprop="description"]::text'
    css_rent = '.prix-annonce::text'

    def get_ref(self, item):
        return re.findall('(\d+).htm', super().get_ref(item))[0]

    def get_rent(self, item):
        return re.findall('(\d+)', super().get_rent(item))[0]


class NexitySpider(AgenceSpider):
    name = 'nexity'
    start_urls = [
        'https://www.nexity.fr/annonces/location/france/1/immobilier/tout?types'
        '_bien=Appartement,Maison/Villa,Loft/Atelier/Surface&type_commercialisa'
        'tion=Location&page=1&pageSize=12&nb_p=2,3,4,5,6,7,8,9,10,11&min_surfac'
        'e=45&max_budget=1000&locationsId[]=23317&sortField=prix&sortOrder=asc'
    ]

    ad_selector = '.annonces > div.item'

    css_ref = '::attr(data-id)'
    css_url = 'a.offer-link::attr(href)'
    css_title = '.title::text'
    css_rent = '.partir span::text'

    def get_url(self, item):
        return 'https://www.nexity.fr' + super().get_url(item)


class BlotSpider(AgenceSpider):
    name = 'blot'
    start_urls = [
        'https://www.blot-immobilier.fr/habitat/location/maison--appartement--'
        'appartement-neuf/loire-atlantique/nantes/?t2=true&t3=true&t4=true'
    ]

    ad_selector = '.bloc_annonce_habitat'

    css_ref = 'a.btn_contact[data-id_annonce]::attr(data-id_annonce)'
    css_url = '.visuel a::attr(href)'
    css_title = 'h4::text'
    css_rent = '.prix strong::text'
    css_rooms = '.chiffres_cles span strong::text'
    css_surface = '.chiffres_cles span:nth-child(2) strong::text'

    def get_url(self, item):
        return 'https://www.blot-immobilier.fr' + super().get_url(item)


class LAdresseSpider(AgenceSpider):
    name = 'ladresse'
    start_urls = [
        'http://www.ladresse.com/catalog/advanced_search_result.php?action='
        'update_search&search_id=&map_polygone=&C_65_search=CONTIENT&C_65_type='
        'TEXT&C_65=44+NANTES&C_65_temp=44+NANTES&C_28_search=EGAL&C_28_type='
        'UNIQUE&C_28=Location&C_27_search=EGAL&C_27_type=TEXT&C_27=&C_30_search'
        '=COMPRIS&C_30_type=NUMBER&C_30_MIN=&C_30_MAX=1000&30_MIN=&30_MAX=1+000'
        '&34min=&34max=&36min=&36max=&40min=&40max=&51min=&51max=&keywords='
    ]

    ad_selector = '.wrap-entry'

    css_ref = '[data-product-id]::attr(data-product-id)'
    css_url = 'a::attr(href)'
    css_title = '.item-ville::text'
    css_rent = '.item-price::text'
    css_rooms = '.item-name::text'
    css_surface = '.item-name::text'

    def get_url(self, item):
        return 'http://www.ladresse.com' + super().get_url(item)[2:]

    def get_rent(self, item):
        return re.findall('(\d+)', super().get_rent(item))[0]

    def get_rooms(self, item):
        findall = re.findall('(\d) PIÈCE', super().get_surface(item))
        return findall[0] if findall else ''

    def get_title(self, item):
        return super().get_title(item).replace('  ', '')

    def get_surface(self, item):
        findall = re.findall('([\d\.]+) M2', super().get_surface(item))
        return findall[0] if findall else ''


class MGeffraySpider(AgenceSpider):
    name = 'mgeffray'
    start_urls = [
        'https://www.mgeffray-immobilier.fr/location/all/all/all/all/1000-'
        'euros-maximum'
    ]

    ad_selector = '.node-location'
    next_page_selector = '.pager-next a::attr(href)'

    css_ref = '::attr(id)'
    css_url = 'a::attr(href)'
    css_title = 'h2 a::text'
    css_rent = '.prix::text'

    def get_ref(self, item):
        return super().get_ref(item)[5:]

    def get_url(self, item):
        return 'https://www.mgeffray-immobilier.fr' + super().get_url(item)

    def get_rent(self, item):
        return re.findall('(\d+)', super().get_rent(item))[0]
