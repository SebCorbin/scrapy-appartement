import re
from collections import defaultdict

from pygsheets import format_addr
from scrapy import Request

from apt.settings import FIREFOX_USER_AGENT
from apt.spiders import AbstractSpider, NB_FIELDS, REF, DATE, RENT, ROOMS, \
    SURFACE, FLOOR, ENERGY_CLASS, KITCHEN, PARKING, detect_parking, EXTERIOR, \
    TITLE, LIFT, MAP, detect_cuisine, detect_exterior


class SeLogerSpider(AbstractSpider):
    name = "seloger"
    start_urls = [
        'https://www.seloger.com/list.htm?types=1,2&projects=1&furnished=0&'
        'price=NaN/1000&surface=50/NaN&rooms=2,3,4&places=[{ci:440109}]&'
        'qsVersion=1.0'
    ]
    ws_title = "SeLoger"
    custom_settings = {
        'ROBOTSTXT_OBEY': False,
        'USER_AGENT': FIREFOX_USER_AGENT
    }
    ad_selector = '.liste_resultat div.cartouche'
    next_page_selector = '.pagination-next::attr(href)'

    def get_ad_info(self, item):
        current_ref = item.css('::attr(data-listing-id)').extract()[0]
        url = item.css('a.c-pa-link::attr(href)').extract_first()
        url = url[:url.find('?')]
        return current_ref, url

    def parse_apt(self, response):
        values = [None for _ in range(NB_FIELDS - 1)]
        script_val = response.xpath('//head/script[9]/text()').extract()[0]

        """
        What we need to parse
        Object.defineProperty( ConfigDetail, 'dpeL', {
          value: "D",
          enumerable: true
        });
        """
        pattern = r'ConfigDetail, \'(.+)\', {\s+value: "(.+)",'
        data = defaultdict(
            str, re.compile(pattern, re.MULTILINE).findall(script_val)
        )

        values[REF] = '=HYPERLINK("%s";"%s")' % (
            response.url, data['idAnnonce']
        )
        values[DATE] = ''  # data['first_publication_date']
        values[RENT] = data['price']

        values[ROOMS] = data['nbPieces']
        values[SURFACE] = data['surfaceT']
        values[FLOOR] = data['etage']
        values[ENERGY_CLASS] = data['dpeL']

        body = data['descriptionBien'].replace('\r\n', '\n')
        values[KITCHEN] = data['idTypeCuisine']
        values[PARKING] = detect_parking(body)
        values[EXTERIOR] = ''
        if data['balcon'] is not '':
            values[EXTERIOR] = 'Balcon' if data['balcon'] == '1' else 'Terrasse'

        values[TITLE] = data['objectMail']

        values[FLOOR] = ''
        values[LIFT] = ''
        values[MAP] = ''
        if data['mapModeAffichage'] == 'LatLng':
            values[MAP] = '=HYPERLINK("%s/%s,%s/";"Lieu")' % (
                'https://www.google.fr/maps/search',
                data['mapCoordonneesLatitude'],
                data['mapCoordonneesLongitude']
            )

        crange = response.meta['crange']
        self.sh.update_cells(crange, [values])
        title_cell_addr = format_addr((format_addr(crange)[0], TITLE + 1))
        title_cell = self.sh.cell(title_cell_addr)
        assert title_cell.value == data['objectMail']
        title_cell.simple = False
        title_cell.note = body


class AcheterLouerSpider(AbstractSpider):
    name = "acheterlouer"
    ws_title = "AcheterLouer"
    custom_settings = {
        'ROBOTSTXT_OBEY': False,
        'COOKIES_DEBUG': True,
        'USER_AGENT': FIREFOX_USER_AGENT
    }
    ad_selector = 'div.adv_idannonce'
    next_page_selector = 'a.next::attr(href)'

    def get_ad_info(self, item):
        current_ref = re.findall('\d+', item.css('::attr(id)').extract()[0])
        url = item.css('a.adv_nbPhoto::attr(href)').extract_first()
        return current_ref, url

    def parse_apt(self, response):
        values = [None for _ in range(NB_FIELDS - 1)]

        values[REF] = '=HYPERLINK("%s";"%s")' % (
            response.url, response.css('[name=idannonce]').extract_first()
        )

        values[DATE] = ''  # data['first_publication_date']
        values[RENT] = response.css('.adv_srch_price::text').extract_first()

    def parse(self, response):
        if 'aucun r' in str(response.body):
            print('FAIL')
        return super().parse(response)

    def start_requests(self):
        yield Request(
            'https://www.acheter-louer.fr/recherche.htm?page=1&tri=2&'
            'localisation=NANTES+%2844000%29&location=1&maison=1&appartement=1&'
            'surfaceh_min=50&surfaceh_max=&F2=1&F3=1&surfaceterr_min=&'
            'surfaceterr_max=&prixa=&prixb=1000&motscles=&etou=OR',
            cookies={
                '_isBrowser': 'true',
                'Cookie_acheterlouer': "1",
                'htl_search': "293b8e444eafdb0385120768cb3eb43a",
                'acheter-louer_pub0': "4023%3B3769",
                'acheter-louer_pub1': "4096%3B4029%3B3787%3B4027",
            },
            dont_filter=True
        )


class AVendreALouerSpider(AbstractSpider):
    name = "avendrealouer"
    ws_title = "AVendreALouer"
    custom_settings = {
        'ROBOTSTXT_OBEY': False,
    }
    ad_selector = 'ul#result-list li[data-adid]'
    next_page_selector = 'a.next::attr(href)'
    start_urls = [
        'https://www.avendrealouer.fr/recherche.html?pageIndex=1&'
        'sortPropertyName=Price&sortDirection=Descending&searchTypeID=2&'
        'typeGroupCategoryID=6&transactionId=2&localityIds=101-20180&'
        'typeGroupIds=47,48&maximumPrice=1000&minimumSurface=50&housingIds=0&'
        'roomComfortIds=2,3,4,5&hasMoreCriterias=true'
    ]

    def get_ad_info(self, item):
        current_ref = item.css('::attr(data-adid)').extract()[0]
        url = item.css('a.picCtnr::attr(href)').extract_first()
        return current_ref, url

    def parse_apt(self, response):
        values = [None for _ in range(NB_FIELDS - 1)]

        values[REF] = '=HYPERLINK("%s";"%s")' % (
            response.url, self.extract_id(response, 'Contact_PropertyId')
        )

        values[SURFACE] = self.extract_id(response, 'PropertyLivingSurface')
        values[RENT] = response.css('#fd-price-val::text').extract_first()

        mapping = {
            'Pièce(s)': ROOMS,
            'Etage du bien': FLOOR,
            'Parking': PARKING,
            'Ascenseur': LIFT,
        }
        for span in response.css('#table td span:first-child'):
            text = span.css('::text').extract_first()
            key = [v for k, v in mapping.items() if k in text]
            if key:
                if key[0] == LIFT:
                    values[key[0]] = 'Oui'
                else:
                    xpath_selector = 'following-sibling::span/text()'
                    values[key[0]] = span.xpath(xpath_selector)[0].extract()

        body = response.css('#propertyDesc::text').extract_first()
        values[DATE] = re.findall('Mise à jour le (\d{2}/\d{2}/\d{4})', body)[0]
        energy = response.css('#energysymbol::text')
        if energy:
            values[ENERGY_CLASS] = energy.extract()[0]

        values[KITCHEN] = detect_cuisine(body)
        values[PARKING] = detect_parking(body)
        values[EXTERIOR] = detect_exterior(body)
        values[TITLE] = response.css('.fd-title .mainh1::text').extract_first()
        neighborhood = response.css('.district::text')
        if neighborhood:
            values[MAP] = neighborhood.extract_first()

        crange = response.meta['crange']
        self.sh.update_values(crange, [values])
        title_cell_addr = format_addr((format_addr(crange)[0], TITLE + 1))
        title_cell = self.sh.cell(title_cell_addr)
        assert title_cell.value == values[TITLE]
        title_cell.simple = False
        title_cell.note = body

    @staticmethod
    def extract_id(response, elem_id):
        return response.css('#%s::attr(value)' % elem_id).extract()[0]
